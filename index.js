/*
 HIVE Rewards Auto Claim Bot v1.0 - by @drakos

 FEATURES:
 - Uses the multiplatform NodeJS
 - Support for multiple accounts
 - Error handling of wrong account names
 - RPC node status check
 - Private/public key validation

 SETUP:
 - Install NodeJS (https://nodejs.org).
 - Create a folder for your project and go into it.
 - Copy the script into a file, e.g. claim_rewards.js
 - Alternatively `git clone https://github.com/Jolly-Pirate/steem-claim-rewards`, then `cd steem-claim-rewards`
 - Edit the CONFIG section.
 - Under linux, secure the file's permissions: `chmod 600 claim_rewards.js`
 - Install the steem package into your project folder: `npm install steem@latest --save`
 - Run the script from the project's folder with: `node claim_rewards.js`

 NOTE:
 The script file will contain your private posting keys.
 SECURE it, it's your responsibility.
 */

const hive = require('@hiveio/hive-js');
const fs = require('fs')

// CONFIG START
// Add the account names with their corresponding PRIVATE POSTING KEY, separated by commas
// Notice that the last account does not have a comma at the end of its line
var accountsData = fs.readFileSync('/run/secrets/accounts.json');
var interval = process.env.INTERVAL_IN_MINUTES || 1; // Set the timer in minutes
var rpc = process.env.RPC || 'https://api.hive.blog'; // Set your RPC node, or leave api.hive.blog, it's very fast
// CONFIG END

// Sort the accounts alphabetically, keep things nice and ordered
const myAccounts = JSON.parse(accountsData);

// Put the account names into an array
var accountArray = [];
for (const account of Object.keys(myAccounts)) {
    accountArray.push(account); // Append the accounts into a new array
}

getRewardsForAccounts(); // Get the rewards upon running the script, then start the timer
var tid = setInterval(getRewardsForAccounts, interval * 60 * 1000);

// FUNCTIONS

function getRewardsForAccounts() {
    // Print the current date/time (optional)
    var date = new Date().toLocaleDateString();
    var time = new Date().toLocaleTimeString();
    console.log('\n' + date, time);

    // Check if the RPC is up, then get the rewards for each account
    checkRPC(rpc, function (rpc_status) {
        if (rpc_status) {
            accountArray.forEach(function (account) {
                getRewards(account);
            });
        } else {
            console.log(rpc, 'unreachable. Check the address or try another one.');
        }
    });
}

function abortTimer() { // Can be called if you want to stop the timer
    clearInterval(tid);
}

function checkRPC(rpc, callback) {
    hive.api.setOptions({url: rpc});
    hive.config.set('rebranded_api','true');
    hive.broadcast.updateOperations();
    hive.api.getAccountCount(function (err, result) {
        if (err) {
            console.log(err);
            callback(false);
        }
        if (result && result > 0)
            callback(true);
    });
}

function checkPrivateKey(privateKeyFromConfig, publicKeyFromBlockchain) {
    // Verify the private key in the config vs the public key on the blockchain
    try {
        if (hive.auth.wifIsValid(privateKeyFromConfig, publicKeyFromBlockchain)) {
            return true;
        }
    } catch (e) {
        //console.log(e);
        return false;
    }
}

function getRewards(account) {
    hive.api.getAccounts([account], function (err, response) {
        if (err) {
            console.log(err);
        }
        if (response[0]) { // Check the response[0], because the response array is empty when the account doesn't exist on the blockchain
            console.log(response[0]);
            name = response[0]['name'];
            reward_hbd = response[0]['reward_hbd_balance'] || response[0]['reward_sbd_balance']; // will be claimed as Hive Dollars (HBD)
            reward_hive = response[0]['reward_hive_balance'] || response[0]['reward_steem_balance'] ; // this parameter is always '0.000 HIVE'
            reward_hivepower = response[0]['reward_vesting_hive'] || response[0]['reward_vesting_steem']; // HIVE to be received as Hive Power (HP), see reward_vesting_balance below
            reward_vests = response[0]['reward_vesting_balance']; // this is the actual VESTS that will be claimed as HP

            rsbd = parseFloat(reward_hbd);
            rspw = parseFloat(reward_hivepower); // Could also check for reward_vesting_balance instead

            // Claim rewards if there is HBD and/or HP to claim
            if (rsbd > 0 || rspw > 0) {
                privateKey = myAccounts[name]; // Pulled from the JSON object in the CONFIG
                publicKey = response[0].posting.key_auths[0][0]; // Get public key on the blockchain

                // Claim rewards if the private key has a valid public key on the blockchain
                if (checkPrivateKey(privateKey, publicKey)) {
                    // We can claim partial rewards, if we specify the amount of reward_hbd and reward_vesting_balance.
                    // However, we want to claim everything.

                    //steem.broadcast.claimRewardBalance(privateKey, name, reward_hive, '0.005 HBD', '10.000000 VESTS', function (err, response) { // for testing
                    hive.broadcast.claimRewardBalance(privateKey, name, reward_hive, reward_hbd, reward_vests, function (err, response) {
                        if (err) {
                            console.error(err);
                            console.log('Error claiming reward for', account);
                        }
                        if (response) {
                            operationResult = response.operations[0][1]; // Get the claim_reward_balance JSON
                            console.log(operationResult);
                            confirm_account = operationResult.account;
                            confirm_reward_hbd = operationResult.reward_hbd;
                            confirm_reward_vests = operationResult.reward_vests;
                            console.log(confirm_account, 'claimed', confirm_reward_hbd, 'and', rspw.toFixed(3), 'HP (', confirm_reward_vests, ')');
                        }
                    });
                } else {
                    console.log('Invalid private key for:', name);
                }
            }
        } else {
            console.log(account, 'does not exist on the blockchain.');
        }
    });
}
